import 'normalize.css/normalize.css';
import './src/scss/index.scss';

import React from 'react';
import { render } from 'react-dom';

import App from './src/App.jsx';

render(
    <App />,
    document.getElementById('app')
);

/**
 * Print information on the console.
 * 'APP_VERSION' and 'APP_NAME' is defined in webpack config file.
 */
console.info(`%c${APP_NAME}%c
Version: ${APP_VERSION}
Copyright © ${new Date().getFullYear()}`,
'font-size: 18px; color: #3b3e43;',
'font-size: 12px; color: rgba(0,0,0,0.38);');
