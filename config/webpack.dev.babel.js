/**
 * Webpack Development Environment Configuration.
 */
import path from 'path';

import common from './webpack.config.base';

module.exports = {
    ...common,

    mode: 'development',

    devtool: 'cheap-module-source-map',

    module: {
        rules: [
            ...common.module.rules,

            {
                test: /\.(s?css|sass)$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: true
                        }
                    }, {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                config: path.join(__dirname, 'postcss.config.js')
                            }
                        }
                    },
                    'sass-loader'
                ]
            }
        ]
    },

    // webpack-dev-server config
    devServer: {
        host: '0.0.0.0',
        port: 7991,
        hot: true,
        compress: true,
        static: {
            directory: path.resolve(__dirname, '..')
        },
        devMiddleware: {
            stats: 'errors-only'
        },
        client: {
            overlay: true
        }
    }
}
