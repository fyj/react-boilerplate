module.exports = {
    plugins: {
        autoprefixer: {
            overrideBrowserslist: [
                '> 1%',
                'last 2 version',
                'not ie <= 8'
            ]
        }
    }
}
