/**
 * Webpack Development Common Configuration.
 */
import path from 'path';

import webpack from 'webpack';

import pkg from '../package.json';

export default {
    entry: {
        fyj: './index.js'
    },

    output: {
        path: path.resolve(__dirname, '..', 'dist'),
        filename: '[name].min.js'
    },

    plugins: [
        new webpack.DefinePlugin({
            APP_NAME: `'${pkg.name}'`,
            APP_VERSION: `'${pkg.version}'`
        })
    ],

    resolve: {
        modules: ['node_modules'],
        extensions: ['.js', '.jsx', '.css', '.scss']
    },

    module: {
        rules: [{
            test: /\.m?js$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                    ]
                }
            }],
            exclude: /node_modules/
        }, {
            test: /\.jsx?$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-env',
                        '@babel/preset-react'
                    ]
                }
            }],
            exclude: /node_modules/
        }]
    }
}
