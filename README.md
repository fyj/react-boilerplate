# React Boilerplate

> A react boilerplate to facilitate website development.

[![GitLabCI](https://img.shields.io/gitlab/pipeline/xn-02f/react-boilerplate/master.svg?style=flat-square&logo=gitlab)](https://gitlab.com/xn-02f/react-boilerplate/pipelines)

[Preview Live](https://xn-02f.gitlab.io/react-boilerplate)
