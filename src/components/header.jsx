import React, { Component } from 'react';

class Header extends Component {
    constructor () {
        super();

        this.state = {
            now: new Date()
        }
    }

    render() {
        return (
            <div>
                <h6>{this.state.now}</h6>
            </div>
        );
    }
}

export default Header;
