import React, { Component } from 'react';

class App extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div className="main">
                <h1>React boilerplate</h1>
                <h3>React + Babel + Webpack</h3>
            </div>
        );
    }
}

export default App;
